import requests
import schedule
from schedule import every, repeat


@repeat(every(1).day.at("09:00"))
@repeat(every(1).day.at("17:00"))
@repeat(every(1).day.at("18:00"))
def trigger_emails():
    print("Triggering emails")
    response = requests.post(
        "http://https://terms-qynk.onrender.com/budget/v1/alert/trigger-email/"
    )
    print(response.json())


while True:
    schedule.run_pending()
